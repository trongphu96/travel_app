package com.ptit.trongphu.travel_app.objects;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "events_new")
public class ChecklistItem {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private  int mId = 0;

    @ColumnInfo(name = "name")
    private final String mName;

    @ColumnInfo(name = "isDone")
    private final String mIsDone;


    public ChecklistItem(String name, String isDone) {
        this.mName = name;
        this.mIsDone = isDone;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getIsDone() {
        return mIsDone;
    }

    public void setId(int id) {
        this.mId = id;
    }
}