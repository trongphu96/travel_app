package com.ptit.trongphu.travel_app.project_travel.login;

import android.os.Handler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.ptit.trongphu.travel_app.utils.Constants.BASE_URL;


class LoginPresenter {
    private LoginView mView;

    public void bind(LoginView view) {
        this.mView = view;
    }

    public void unbind() {
        mView = null;
    }

    public void signUp() {
        mView.openSignUp();
    }


    public void ok_signUp(final String firstname, final String lastname, final String email,
                          String pass, final Handler mhandler) {

        mView.showLoadingDialog();

        String uri = BASE_URL + "sign-up";

        //Set up client
        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("email", email)
                .addFormDataPart("password", pass)
                .addFormDataPart("firstname", firstname)
                .addFormDataPart("lastname", lastname)
                .build();

        //Execute request
        final Request request = new Request.Builder()
                .url(uri)
                .post(requestBody)
                .build();

        //Setup callback
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mhandler.post(() -> {
                    mView.showError();
                    mView.dismissLoadingDialog();
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String res = Objects.requireNonNull(response.body()).string();
                final int responseCode = response.code();
                mhandler.post(() -> {
                    try {
                        String successfulMessage = "\"Successfully registered\"";
                        if (responseCode == HttpsURLConnection.HTTP_CREATED && res.equals(successfulMessage)) {
                            //if successful redirect to login
                            mView.openLogin();
                            mView.setLoginEmail(email);
                            mView.showMessage("signup succeeded! please login");
                        } else {
                            // show error message
                            mView.showMessage(res);
                        }
                        mView.dismissLoadingDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                        mView.showError();
                    }
                });
            }
        });
    }

    public void login() {
        mView.openLogin();
    }


    public void ok_login(final String email, String pass, final Handler mhandler) {

        mView.showLoadingDialog();

        String uri = BASE_URL + "sign-in";

        //Set up client
        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", email)
                .addFormDataPart("password", pass)
                .build();

        //Execute request
        Request request = new Request.Builder()
                .url(uri)
                .post(requestBody)
                .build();
        //Setup callback
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mhandler.post(() -> {
                    mView.showError();
                    mView.dismissLoadingDialog();
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String res = Objects.requireNonNull(response.body()).string();
                mhandler.post(() -> {
                            try {
                                if (response.isSuccessful()) {
                                    JSONObject responeJsonObject = new JSONObject(res);
                                    String token = responeJsonObject.getString("token");
                                    mView.rememberUserInfo(token, email);
                                    mView.startMainActivity();
                                } else {
                                    mView.showError();
                                }
                                mView.dismissLoadingDialog();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                );
            }
        });
    }

}