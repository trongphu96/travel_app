package com.ptit.trongphu.travel_app.project_travel.destinations.funfacts;

import java.util.ArrayList;

import com.ptit.trongphu.travel_app.objects.FunFact;

/**
 * Created by niranjanb on 14/06/17.
 */

interface FunFactsView {
    void showProgressDialog();
    void hideProgressDialog();
    void setupViewPager(ArrayList<FunFact> factsArray);
}
