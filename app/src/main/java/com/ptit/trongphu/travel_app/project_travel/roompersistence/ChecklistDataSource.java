package com.ptit.trongphu.travel_app.project_travel.roompersistence;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import com.ptit.trongphu.travel_app.objects.ChecklistItem;

/**
 * Access point for using ChecklistItem data using
 * room com.ptit.trongphu.travel_app.database
 */
public class ChecklistDataSource {

    private final ChecklistItemDAO mDao;

    public ChecklistDataSource(ChecklistItemDAO dao) {
        mDao = dao;
    }

    Flowable<List<ChecklistItem>> getSortedItems() {
        return mDao.getSortedItems();
    }

    void updateIsDone(int id) {
        mDao.updateIsDone(id);
    }

    void updateUndone(int id) {
        mDao.updateUndone(id);
    }

    void insertItem(ChecklistItem item) {
        mDao.insertItems(item);
    }

    void deleteCompletedTasks() {
        mDao.deleteCompletedTasks();
    }

    Single<List<ChecklistItem>> getCompletedItems() {
        return  mDao.getCompletedItems();
    }
}
