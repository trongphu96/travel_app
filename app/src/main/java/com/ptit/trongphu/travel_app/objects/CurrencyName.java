package com.ptit.trongphu.travel_app.objects;

public class CurrencyName {
    public String shortName;
    public String abrivation;

    public CurrencyName(String shortName, String abrivation) {
        this.shortName = shortName;
        this.abrivation = abrivation;
    }
}
